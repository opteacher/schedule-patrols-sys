import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/schedule-patrols-sys/page/login',
    name: 'Login',
    component: () => import('../views/Login.vue')
  },
  {
    path: '/schedule-patrols-sys/page/schedule',
    name: 'Schedule',
    component: () => import('../views/Schedule.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
